using System;

namespace laba2.DataClasses
{
    public class Calls
    {
        public int id { get; set; }
        public Person user { get; set; }
        public DateTime date_of_calls { get; set; }
        public float duration { get; set; }
        public City city { get; set; }
    }
}