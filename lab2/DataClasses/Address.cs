using System.Collections.Generic;

namespace laba2.DataClasses
{
    public class Address
    {
        public int id { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string house_number { get; set; }
        public List<Person> persons { get; set; }
    }
}